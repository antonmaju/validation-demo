﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ValidationTraining.Core;
using ValidationTraining.Models;

namespace ValidationTraining.Controllers
{
    public class HomeController : Controller
    {
        private TrainingDbContext dbContext;

        public HomeController()
        {
            dbContext = new TrainingDbContext(); 
        }

        /// <summary>
        /// Index action
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var soldier = new Soldier();

            return View(soldier);
        }

        /// <summary>
        /// handles soldier submission through normal page POST
        /// </summary>
        /// <param name="soldier">soldier info</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(Soldier soldier)
        {
            if (! ModelState.IsValid || !ValidateSoldier(soldier))
            {
                return View(soldier);
            }

            dbContext.Soldiers.Add(soldier);
            dbContext.SaveChanges();

            TempData["message"] = "Data was saved successfully!";

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Handles soldier submission through AJAX
        /// </summary>
        /// <param name="soldier">soldier info</param>
        /// <returns>J { "success": true/false, "errors": [] }  </returns>
        [HttpPost]
        public ActionResult SaveSoldier(Soldier soldier)
        {
            if (!ModelState.IsValid || !ValidateSoldier(soldier))
            {
                //wraps model state error to list of ErrorInfo instances
                //and returns it in json format
                var errorStates = ModelState.Where(m => m.Value.Errors.Count > 0)
                    .Select(m => Tuple.Create(m.Key, m.Value.Errors)).ToList();
                var errorList = new List<ErrorInfo>();

                foreach (var tuple in errorStates)
                {
                    foreach (var error in tuple.Item2)
                    {
                        errorList.Add(new ErrorInfo
                        {
                            Property = tuple.Item1,
                            Message = error.ErrorMessage
                        });
                    } 
                }

                //returns {"success":false, "errors": [ ... ]}
                //bisa dicoba cek di web developer toolsnya Chrome atau Firebugnya Firefox
                return Json(new {success = false, errors = errorList});
            }

            dbContext.Soldiers.Add(soldier);
            dbContext.SaveChanges();

            //returns {"success":true}
            return Json(new {success = true});
        }

        bool ValidateSoldier(Soldier soldier)
        {
            //check if email is duplicate
            
            var existing =  dbContext.Soldiers.FirstOrDefault(s => s.Email == soldier.Email);
        
            if(existing != null)
               ModelState.AddModelError("Email","Email is duplicate");

            return ModelState.IsValid;
        }


    }
}
