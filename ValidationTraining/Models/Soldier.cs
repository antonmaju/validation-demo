﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ValidationTraining.Core;

namespace ValidationTraining.Models
{
    public class Soldier
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [Email]
        public string Email { get; set; }

        [Range(1,100)]
        public int Health { get; set; }

        [Range(1,100)]
        public int Stamina { get; set; }

    }
}