﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ValidationTraining.Models
{
    public class ErrorInfo
    {
        public string Property { get; set; }

        public string Message { get; set; }
    }
}