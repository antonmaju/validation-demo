﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace ValidationTraining.Core
{
    public static class CommonHelpers
    {
        public static MvcHtmlString ValidationClassFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> propertyExpression,
            string className = "has-error")
        {
            string value = string.Empty;

            if (propertyExpression.Body.NodeType != ExpressionType.MemberAccess)
                throw new ArgumentException();

            var memberInfo = (propertyExpression.Body as MemberExpression).Member;

            if (!helper.ViewData.ModelState.ContainsKey(memberInfo.Name))
                return new MvcHtmlString(value);


            if (helper.ViewData.ModelState[memberInfo.Name].Errors.Count > 0)
                value = className;

            return new MvcHtmlString(value);
        }

    }
}