﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ValidationTraining.Models;

namespace ValidationTraining.Core
{
    public class TrainingDbContext : DbContext
    {
        public TrainingDbContext(): base("TrainingDb")
        {
        
        }

        public DbSet<Soldier> Soldiers { get; set; }
    }
}