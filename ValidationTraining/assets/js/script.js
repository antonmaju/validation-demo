﻿$(function() {

    $('#btnShowModal').click(function() {
        var $entry = $('#modal-entry');

        //remove previous validation state
        $('.form-group', $entry).removeClass('has-error');
        $('.field-validation-error', $entry).empty();

        //clear all values
        $('input[type=text]', $entry).val('');

        $entry.modal('show');
    });

    $('#btnSaveModal').click(function() {
        var $entry = $('#modal-entry');

        //remove previous validation state
        $('.field-validation-error', $entry).empty();
        $('.form-group', $entry).removeClass('has-error');


        var soldier = {
            Name: $('#txtName').val(),
            Email: $('#txtEmail').val(),
            Health: $('#txtHealth').val(),
            Stamina: $('#txtStamina').val()
        };


        $.post('/Home/SaveSoldier', soldier).done(function(result) {
            
            if (result.success) {
                $entry.modal('hide');
                alert('Data was saved successfully');
            } else {
                //display errors 
                $.each(result.errors, function (index, error) {
                    $('.form-group', $entry).filter('[data-property=' + error.Property + ']').addClass('has-error');
                    $('.field-validation-error', $entry).filter('[data-property=' + error.Property + ']').html(error.Message);
                });
            }   

        }).fail(function(result) {
            alert('Failed to save data !');
        });
    });


});