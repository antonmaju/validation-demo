[assembly: WebActivator.PreApplicationStartMethod(typeof(ValidationTraining.App_Start.SquishItLess), "Start")]

namespace ValidationTraining.App_Start
{
    using SquishIt.Framework;
    using SquishIt.Less;

    public class SquishItLess
    {
        public static void Start()
        {
            Bundle.RegisterStylePreprocessor(new LessPreprocessor());
        }
    }
}